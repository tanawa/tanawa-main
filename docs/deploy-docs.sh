#!/bin/bash

# Clean
rm -rf target/

# Build site
cd doc-src
hugo
cd ..

# Clone docs repo
git clone git@bitbucket.org:tanawa/tanawa.bitbucket.org.git target

# Copy site to repo
echo "Copying site to repo..."
cp -rf doc-src/public/* target

echo "Updated site in local repo, ready to commit and push manually"
cd target/
