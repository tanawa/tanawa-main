+++
title = "Developer Handbook"
+++

## Introduction

The developer handbook should support you when developing parts of the tanawa application. It provides information about what you need to build the software on your local machine.
It also provide information about our architectural vision and how to implement certain things.

## Development build
Activator is used to setup and build the application. On first use simply execute the "activator" shell script. It will trigger and SBT build and download all needed dependencies.

Use run within the activator console to start the application and Ctrl+D to stop it again. As long as no additional dependencies are required, the incremental compiler does a great job.
This means, you can change the code without having to explicitly deploy new versions of it. If you add a dependency you might stop the application and activator and restart it.

### Debugging
Simple run activator with -jvm-debug=9999 to listen on port 9999 for debugging commands.

## Spring Data Neo4j - Object graph mapping
The mapping between the models and the graph nodes is done via spring-data-neo4j. To be able to control lazy loading and other advanced features, compile time weaving is used.
As the AspectJ compiler does not understand Java, and needs source code instead of JVM byte code as input, the models have to be written in Java. The rest of the application is written in Scala.

The data is retrieved ove repository services, which are provided as spring beans. A repository is what we called DAO data access object in JEE.

### Profile image storage
There exist currently two implementations for storing the profile images of a godchild:

  - Neo4jGodchildService: This is the default implementation and is activated by the Spring "default" environment profile.
  - FileStorageGodchildService: This is an additional implementation for storing the images external on the local disk. This implementation gets activated by the Spring env profile "localfilebased", which would replace the default implementation.
  
both implementations extend the trait _GodchildService_.


## JSON
Jackson is used to convert the models to JSON. The play framework would provide a nice, typesafe way to convert from and to JSON.
Unfortunately it requires implicits, which are only available in Scala, but our models are written in Java due to limitations of the AspectJ compiler.
Controllers which would like to provide JSON data, might extend the JsonResult trait.

## Views
Only a view views need to be written. Backend and frontend mostly communicate over JSON. All remaining views are Scala Templates.

## Spring integration
For spring to do dependency injection in a class, it needs to control it. Therefore instantiation of controllers needs to be delegated to spring.
To do so the Global object is overwritten. To delegate controller instantiation to spring the path to the controller, within the routes.conf, needs to be prefixed with an @ sign.

## Testing
We separate three kind of tests. Unit tests, integration tests and frontend tests.

### Unit tests
Unit tests focus on small and specific parts of the application. They never test more than one class at a time. Consequently mocking is heavily used.

### Integration tests
Integration tests test against a running instance of tanawa. No mocking is used. Although tests should focus on certain functions, the whole stack is tested with every request.

### Frontend tests
With the frontend containing big parts of the applications functionality it is crucial to test it separately and explicitly.
Framework #TODO define testing framework
is used to do so.

## Continuous Integration
Still to be defined

  - http://www.cloudbees.com/
  - https://travis-ci.org/
  - Bamboo
  
## Deployment

Runs on Heroku: 

  - Development: [http://tanawa-dev.herokuapp.com/][heroku-dev]

Before you can deploy, you have to install the heroku cli, login and create the heroku remote.

  - Deploy the subfolder webapp with git subtree: `git subtree push --prefix webapp heroku master`
  - If you get rejected because of your branch is behind, just force the push:  git push heroku \`git subtree split --prefix webapp master\`:master --force

Git Subtree tipp from: https://coderwall.com/p/ssxp5q


[heroku-dev]: http://tanawa-dev.herokuapp.com/
