+++
title = "Architecture"
+++

## Vision and Values

The Tanawa system helps relief/non-profit organizations managing easily their resources, such as godchildren and their donors.

We focus primarily on:

  - Simplicity: The system should be kept simple, the backend as well the frontend.
  - Maintainability: The system should be well-structured and easy understandable.
  - Extensibility: The system should be open for extensions and customizations.
  - Innovation: The system should use the latest web technologies.

  
## Overview

![Tanawa Architecture overview][tanawa-architecture-overview]


[tanawa-architecture-overview]: /images/tanawa-architecture-overview.jpg

## Technology

A modern yet proofed open-source technology stack was chosen. The play framework www.playframework.org was chosen as foundation. With Scala as the programming language of choice.
The data resides in a Neo4j (property) graph database www.neo4j.org. Spring www.spring.io was chosen as DI framework. Models and Data are connected by spring-data-neo4j.
Testing is done with ScalaTest using FlatSpec.
The frontend relies heavily on AngularJS www.angularjs.org.


## Security

Usage of the great pac4j library: [http://www.pac4j.org/][pac4j]


[pac4j]: http://www.pac4j.org/
