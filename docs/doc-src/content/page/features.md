+++
weight = "2"
menu = "main"
title = "Features"
+++


## Child management

  - Create/Edit/Delete children
  - Manage children data
  - more to come...

## Donor management

  - Create/Edit/Delete donors
  - Connect donors with children
  - more to come...
