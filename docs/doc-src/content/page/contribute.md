+++
weight = "4"
menu = "main"
title = "Contribute"
+++

Please feel free to contribute <i class="fa fa-beer" style="font-size: 20px"></i>

Thank you!


## Mailing lists

  - For developers: [Join group][tanawa-developers]
  
  
## Bugs, Improvements, etc.

See our Issue Tracker: [Jira][jira]


## Get in touch with our code

See [Bitbucket][bitbucket].


[jira]: https://tanawa.atlassian.net/
[bitbucket]: https://bitbucket.org/tanawa/tanawa-main
[tanawa-developers]: https://groups.google.com/d/forum/tanawa-developers
