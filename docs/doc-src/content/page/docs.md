+++
weight = "3"
menu = "main"
title = "Docs"
+++

## Getting Started

todo

## User manual

todo

## Developer Handbook

  - [Frontend developers][fe-dev]
  - [Scala, Play developers][play-dev]


## Architecture

Please [read here][architecture] the main principles of the architecture of Tanawa.


[architecture]: /page/architecture
[fe-dev]: https://bitbucket.org/tanawa/tanawa-main/src/db2149cadaff278ed4c6d5ff4631806b39718ee6/frontend/README.md?at=master
[play-dev]: /page/developerHandbook
