+++
weight = "2"
menu = "main"
title = "About"
+++
Tanawa is a kind of ERP/CRM system to easily manage donors and children. 
It's modular architecture allows easily to extend the system for further needs of an organization.

"Tan-awa" is Cebuano (a Philippine's language) and means "to see" in English. 

With the "Tanawa" web application the organization should be able to see everything :).

## License

Tanawa is under the version 2.0 of the [Apache License][].

[Apache License]: http://www.apache.org/licenses/LICENSE-2.0
