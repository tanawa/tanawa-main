# Documentation

## Site sources

The docs website is built with the [Hugo][] site generator tool.

Please read the general docs like installation, etc. here: http://gohugo.io/overview/introduction

We use the Bitbucket static website webhosting feature, more infos here: https://confluence.atlassian.com/display/BITBUCKET/Publishing+a+Website+on+Bitbucket


## Edit docs site in Dev mode

  - Change to the _doc-src_ directory, from here you can start then the "hugo" commands
  - Edit, edit, edit files...
  - Excute `hugo server` to preview the site in your browser
  - Or you can also use `hugo server --watch` to watch for changes in your content
  
  For deploying the updated site to the tanawa.bitbucket.org webspace, check the next chapter.

## Build docs site and update website

  - Change to directory _doc-src_
  - Execute command _hugo_ to build the whole site
  - Copy files/folders from _public_ folder to the separate Tanawa site Git repository (see https://bitbucket.org/tanawa/tanawa.bitbucket.org)
  - Push the changes to the separate site repository
  - Site should then be live and up-to-date
  
[Hugo]: http://gohugo.io/