package models

import org.scalatestplus.play.PlaySpec

class AddressSpec extends PlaySpec {

  "The Address model" should {

    "return true if its data is fully defined" in {
      // Given:
      val city = new City("MyTestCity")
      val country = new Country(scala.Option.apply(1L), scala.Option.apply("MyTestCountry"))
      val testee = new Address(scala.Option.apply(1L), scala.Option.apply("TestStreet"), scala.Option.apply("TestPostal2000"), city,
        scala.Option.apply("TestProvince"), country)

      // When:
      // Then:
      testee.isDefined() mustBe true
    }

    "return false if no address data is given" in {
      // Given:
      val city = new City()
      val country = new Country()
      val testee = new Address()

      // When:
      // Then:
      testee.isDefined() mustBe false
    }
  }

}