package services.converter

import org.scalatestplus.play.PlaySpec

class StringBase64ToByteArrayConverterSpec extends PlaySpec {
  
    "The StringBase64ToByteArrayConverter" should {

    "convert a base64 encoded string to a decoded-base64 byte array" in {
      // Given:
      val testee = new StringBase64ToByteArrayConverter
      val base64Data = "dGVzdCBkYXRhIGltYWdl"

      // When:
      val result = testee.convert(base64Data)

      // Then:
      result must equal (Array(116, 101, 115, 116, 32, 100, 97, 116, 97, 32, 105, 109, 97, 103, 101))
    }

  }

}