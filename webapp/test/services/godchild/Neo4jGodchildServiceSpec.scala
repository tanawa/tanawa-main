package services.godchild

import org.mockito.Mockito.verify
import org.mockito.Mockito.stub
import org.scalatestplus.play.PlaySpec
import org.scalatest.mock.MockitoSugar
import models.Godchild
import java.io.File
import models.repository.GodchildRepository
import java.nio.file.Files

class Neo4jGodchildServiceSpec extends PlaySpec with MockitoSugar {

  val testee = new Neo4jGodchildService
  
  "The Neo4j godchild service" should {

    "save a profile image file into Neo4j" in {
      // Given:
      val godchildRepository = mock[GodchildRepository]
      testee.godchildRepository = godchildRepository
      val godchild = mock[Godchild]
      val imgPath = Files.createTempFile("test", "test-profile-image.png") 
      val imgFile = imgPath.toFile()
      val imgData = Files.readAllBytes(imgPath)
      
      // When:
      testee.saveProfileImage(godchild, imgFile)

      // Then:
      verify(godchild).setProfileImage(imgData)
      verify(godchildRepository).save(godchild)
    }

  }

}