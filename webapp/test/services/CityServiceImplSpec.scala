package services

import org.scalatestplus.play.PlaySpec
import models.City
import models.Country
import org.scalatest.mock.MockitoSugar
import models.repository.CityRepository
import org.mockito.Mockito.when
import models.Address

class CityServiceImplSpec extends PlaySpec with MockitoSugar {

  "The City service" should {

    "create a new city object if no existing city can be found with same name" in {
      // Given:
      val city = new City("MyTestCity")
      val country = new Country(scala.Option.apply(1L), scala.Option.apply("MyTestCountry"))
      val address = new Address(scala.Option.apply(1L), scala.Option.apply("TestStreet"), scala.Option.apply("TestPostal2000"), city,
        scala.Option.apply("TestProvince"), country)
      val testee = new CityServiceImpl

      // Set mock
      testee.cityRepository = mock[CityRepository]
      when(testee.cityRepository.findByName(city.getName)) thenReturn null

      // When:
      testee.setUniqueCity(address)

      // Then:
      address.getCity.getName mustBe "MyTestCity"
    }

    "set the existing city if any exist in datastore" in {
      // Given:
      val city = new City("MyTestCity")
      val existingCity = new City("MyExistingTestCity")
      val country = new Country(scala.Option.apply(1L), scala.Option.apply("MyTestCountry"))
      val address = new Address(scala.Option.apply(1L), scala.Option.apply("TestStreet"), scala.Option.apply("TestPostal2000"), city,
        scala.Option.apply("TestProvince"), country)
      val testee = new CityServiceImpl

      // Set mock
      testee.cityRepository = mock[CityRepository]
      when(testee.cityRepository.findByName(city.getName)) thenReturn existingCity

      // When:
      testee.setUniqueCity(address)

      // Then:
      address.getCity.getName mustBe "MyExistingTestCity"
    }

  }

}