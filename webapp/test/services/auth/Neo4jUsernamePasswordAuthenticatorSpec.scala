package services.auth

import org.mockito.Mockito.when
import org.pac4j.http.credentials.UsernamePasswordCredentials
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.PlaySpec
import models.User
import models.repository.UserRepository
import com.google.common.hash.Hashing
import com.google.common.base.Charsets
import org.pac4j.core.exception.CredentialsException

/**
 * @author olimination
 */
class Neo4jUsernamePasswordAuthenticatorSpec extends PlaySpec with MockitoSugar {

  "The Neo4j Authenticator" should {

    "not throw an exception if a user object is found" in {

      // Given:
      val userRepository = mock[UserRepository]
      val sha1Pw = Hashing.sha1().hashString("test", Charsets.UTF_8).toString()
      val username = "admin@mail.com"
      val credentials = new UsernamePasswordCredentials(username, "test", "")
      val testUser = new User()
      testUser.setPassword(sha1Pw)
      testUser.setUsername(username)
      when(userRepository.findByUsernameAndPassword(username, sha1Pw)) thenReturn testUser
      val authenticator = new Neo4jUsernamePasswordAuthenticator()
      authenticator.setUserRepository(userRepository)

      // When:
      // Then:
      noException should be thrownBy authenticator.validate(credentials)
    }

    "throw an exception if no user is found" in {

      // Given:
      val userRepository = mock[UserRepository]
      val sha1Pw = Hashing.sha1().hashString("test", Charsets.UTF_8).toString()
      val username = "admin@mail.com"
      val credentials = new UsernamePasswordCredentials(username, "test", "")
      val testUser = null
      when(userRepository.findByUsernameAndPassword(username, sha1Pw)) thenReturn testUser
      val authenticator = new Neo4jUsernamePasswordAuthenticator()
      authenticator.setUserRepository(userRepository)

      // When:
      // Then:
      an [CredentialsException] should be thrownBy authenticator.validate(credentials)
    }
  }
}