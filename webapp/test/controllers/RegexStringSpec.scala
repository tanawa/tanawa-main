package controllers

import org.scalatest.FlatSpec

/**
 * @author Dani
 */
class RegexStringSpec extends FlatSpec {

  val testee = new RegexString("test string")

  "A RegexString when matching for fragment" should "add .* at the beginning" in {
    assert(testee.asCaseSensitiveFragmentMatch.startsWith(".*"))
  }
  it should behave like fragmentEnding(testee.asCaseSensitiveFragmentMatch)


  "A RegexString when matching for fragment (non case sensitive" should "add (?i).* at the beginning" in {
    assert(testee.asFragmentMatch.startsWith("(?i).*"))
  }
  it should behave like fragmentEnding(testee.asFragmentMatch)

  def fragmentEnding(transformed: => String): Unit = {
    it should "add .* at the end" in {
      assert(transformed.endsWith(".*"))
    }
  }


}
