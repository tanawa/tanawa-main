// Test dataset for tanawa project

// The real system deals with personal information, which needs to be protected.
// Therefore real data cannot be used for testing, especially not in a public open-source project.

// Simpson characters are used as "_Donor:Person" with information taken from http://simpsons.wikia.com
// Futurama characters are used as "_Godchild:Person" with information taken from http://futurama.wikia.com

//wipe database
MATCH (n)
OPTIONAL MATCH (n)-[r]-()
DELETE n, r;

// Users
CREATE (Admin:User:_User {username:'admin@mail.com', password:'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3'})

// Donors
CREATE (Bart:Donor:_Donor:Person {firstName:'Bart', lastName:'Simpson', email:'elbarto@aycaramba.com', notes:'Ay Caramba!'})
CREATE (Homer:Donor:_Donor:Person {firstName:'Homer', lastName:'Simpson', email:'homer@fox.com', notes:'“D\'oh!” ―Homer\'s main Catchphrase “Why you little...!” ―Homer\'s Catchphrase “Woo hoo!” ―Homer\'s catchphrase “Mmm... cake *Drooling*” ―Homer\'s Catchphrase “Stupid Flanders!” ―Homer\'s catchphrase “Shut up Flanders!” ―Homer\'s Catchphrase “AHH!” ―Homer\'s Catchphrase “Lisa! Cut off that infernal racket!” ―Used when Lisa plays her saxophone. “Let\'s all go out for frosty chocolate milkshakes!” ―Used during the Tracey Ullman shorts. “Close, but you\'re way off.” ―Homer\'s catchphrase “Whatever, I\'ll be at Moe\'s.” ―Used during Homer\'s mishaps. “BART!” ―Often used by Homer in the earlier seasons. “LISA!” ―Usually follows "BART!" in the Ullman shorts and earlier episodes. “Aw, come on (insert character here). You used to be cool.”'})
CREATE (Lisa:Donor:_Donor:Person {firstName:'Lisa', lastName:'Simpson', email:'lisa@peace.org'})
CREATE (Edna:Donor:_Donor:Person {firstName:'Edna', lastName:'Krabappel', email:'edna@springfield.edu'})
CREATE (Hibbert:Donor:_Donor:Person {firstName:'Julius', middleName:'Murphy', lastName:'Hibbert', title:'M.D.', email:'hibbert@healthcare.com'})
CREATE (Burns:Donor:_Donor:Person {firstName:'Charles', middleName:'Montgomery', lastName:'Burns', email:'mr_burns@nuclearpower.com'})

// Goldchildren
CREATE (Fry:Godchild:_Godchild:Person {firstName:'Philip', lastName:'Fry', dateBorn:'145666800000', yearBorn:1974
, fatherLastName:'Fry', fatherFirstName:'Yancy', fatherOccupation:'Soldier'
, motherLastName:'Fry née Gleisner', motherOccupation:'none'})
CREATE (Leela:Godchild:_Godchild:Person {firstName:'Leela', lastName:'Turanga', dateBorn:'31743788400000', yearBorn:2975
, fatherLastName:'Turanga', fatherFirstName:'Morris'
, motherLastName:'Turanga', motherFirstName:'Munda'})
CREATE (Bender:Godchild:_Godchild:Person {firstName:'Bender', middleName:'Bending', lastName:'Rodriguez', yearBorn:2996})
CREATE (Amy:Godchild:_Godchild:Person {firstName:'Amy', lastName:'Wong', dateBorn:'31820076000000', yearBorn:2978
, fatherLastName:'Wong', fatherFirstName:'Leonard "Leo"', fatherOccupation:'Owner of the Wong Ranch'
, motherLastName:'Wong', motherFirstName:'Inez', motherOccupation:'Owner of the Wong Ranch'})
CREATE (:Godchild:_Godchild:Person {firstName:'John', middleName:'A.', lastName:'Zoidberg'
, fatherLastName:'Zoidberg', fatherFirstName:'Norm'
, motherLastName:'Zoidberg', motherFirstName:'Sam'})

// Supporting
CREATE
(Bart)-[:SUPPORTS {since:'2012/07/08', __type__:'SupportsRelationship'}]->(Leela),
(Edna)-[:SUPPORTS {since:'1998/10/12', __type__:'SupportsRelationship'}]->(Bender),
(Edna)-[:SUPPORTS {since:'1998/10/12', __type__:'SupportsRelationship'}]->(Amy),
(Lisa)-[:SUPPORTS {since:'2004/09/15', __type__:'SupportsRelationship'}]->(Leela),
(Hibbert)-[:SUPPORTS {since:'1989/02/11', __type__:'SupportsRelationship'}]->(Fry)

// Countries
CREATE (ch:Country:_Country {name:'Switzerland'})
CREATE (de:Country:_Country {name:'Germany'})
CREATE (ph:Country:_Country {name:'Philippines'})
CREATE (us:Country:_Country {name:'United States of America'})

// Cities
CREATE (springfield:City:_City {name: 'Springfield'})

//Address
CREATE (EvergreenT1:Address:_Address {addressStreet:'742 Evegreen Terrace', postalCode:'939'})
CREATE (EvergreenT2:Address:_Address {addressStreet:'Evegreen Terrace', postalCode:'939'})
CREATE (EvergreenT3:Address:_Address {addressStreet:'Evegreen Terrace', postalCode:'939', province:'Springfield\'s State'})
CREATE (BurnsManor:Address:_Address {addressStreet:'1000 Mammon Avenue', postalCode:'939'})

CREATE
(EvergreenT1)-[:HAS_COUNTRY]->(us),
(EvergreenT1)-[:HAS_CITY]->(springfield),
(EvergreenT2)-[:HAS_COUNTRY]->(us),
(EvergreenT2)-[:HAS_CITY]->(springfield),
(EvergreenT3)-[:HAS_COUNTRY]->(us),
(EvergreenT3)-[:HAS_CITY]->(springfield),
(BurnsManor)-[:HAS_COUNTRY]->(us),
(BurnsManor)-[:HAS_CITY]->(springfield)

// Living at
CREATE
(Bart)-[:HAS_ADDRESS]->(EvergreenT1),
(Homer)-[:HAS_ADDRESS]->(EvergreenT2),
(Lisa)-[:HAS_ADDRESS]->(EvergreenT3),
(Burns)-[:HAS_ADDRESS]->(BurnsManor)
