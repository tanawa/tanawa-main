# --- Misc
app.searchResultFor=Search result for
app.home=Home
misc.save=Save
misc.delete=Delete
misc.edit=Edit
misc.detail=View details
misc.detailview=Detail view
misc.dialogs.deleteTitle=Really delete item?
misc.dialogs.deleteText=Do you really want to delete this item?
misc.dialogs.deleteTextHintTitle=Caution:
misc.dialogs.deleteTextHint=The data will be deleted permanently!
sex.male=Male
sex.female=Female

# --- Forms
title=Title
firstName=Firstname
middleName=Middle name
lastName=Lastname
sex=Sex
email=Email
yearBorn=Year of born
yearBorn.help=Use this field if the birthday is unknown. Has to be a numeric value greater or equal to 1900
dateBorn=Birthday
fatherFirstName=Father`s Firstname
fatherLastName=Father`s Lastname
motherFirstName=Mother`s Firstname
motherLastName=Mother`s Lastname
fatherOccupation=Father`s Occupation
motherOccupation=Mother`s Occupation
notes=Notes
form.error=There are form validation errors, please check the red fields!
form.success=Item successfully saved
form.contactdetails=Contact details
form.addressdetails=Address details
form.parentsdetails=Parents details
form.fatherdetails=Father details
form.motherdetails=Mother details
form.additionaldetails=Additional data
form.profileimagelegend=Profile image
form.submitting=Submitting data... Please wait this may take some minutes...
form.viewitem=View item here

# --- Constraints
constraint.required=Required
constraint.email=Required

# --- Errors
error.invalid=Invalid value
error.invalid.java.util.Date=Invalid date value
error.required=This field is required
error.number=Numeric value expected
error.real=Real number value expected
error.real.precision=Real number value with no more than {0} digit(s) including {1} decimal(s) expected
error.min=Must be greater or equal to {0}
error.min.strict=Must be strictly greater than {0}
error.max=Must be less or equal to {0}
error.max.strict=Must be strictly less than {0}
error.minLength=Minimum length is {0}
error.maxLength=Maximum length is {0}
error.email=Valid email required
error.pattern=Must satisfy {0}
error.date=Valid date required
error.uuid=Valid UUID required

# --- Notifications
error=Error
successful=Successful

# --- Godchildren
godchildren=Godchildren
godchildren.create=New
godchildren.edit=Edit
godchildren.list=List all Godchildren
godchildren.searchFor=Search for a Godchild
godchildren.find=Find godchild...
godchildren.withoutDonor=Godchildren without donor

# --- Donors
donors=Donors
donors.detail=Detail view
donors.noSupportedChildren=No children supported
donors.edit=Edit
donors.create=New
donors.list=List donors
donors.searchFor=Search for a Donor
donors.find=Find donor...
donors.withoutGodchild=Donors without godchild
donors.supportedGodchildren=Supported godchildren
address.addressStreet=Street name, number, building, etc.
address.postalCode=Zip/Postal code
address.city.name=City
address.province=Province (or State, Region)
address.country.name=Country
