package controllers

import org.pac4j.play.scala.ScalaController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import models.Godchild
import models.repository.GodchildRepository
import play.api.data.Form
import play.api.data.Forms.date
import play.api.data.Forms.text
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.optional
import play.api.data.Forms.default
import play.api.data.Forms.number
import play.api.i18n.Lang
import play.api.mvc.Action
import play.api.mvc.Controller
import play.api.mvc.Flash
import play.api.i18n.Messages
import play.api.mvc.MultipartFormData
import java.io.File
import play.api.libs.json.Json
import play.api.data.FormError
import play.api.libs.json.Writes
import play.api.libs.json.JsValue
import play.api.mvc.Request
import play.api.mvc.AnyContent
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.FileNotFoundException
import services.godchild.GodchildService
import play.api.mvc.Result
import play.api.mvc.ResponseHeader
import play.api.libs.iteratee.Enumerator
import play.api.libs.Files

/**
 * @author dani
 */
@Component
class Godchildren extends ScalaController with JsonResult with QueryHelper with JavaHelper {
  @Autowired
  var godchildRepository: GodchildRepository = null

  @Autowired
  var godchildService: GodchildService = null
  
  private val profileImageFileParamName = "profileImageFile"

  private val godchildForm: Form[Godchild] = Form(
    mapping(
      "nodeId" -> optional(javaLongNumber),
      "firstName" -> nonEmptyText,
      "middleName" -> optional(text),
      "lastName" -> nonEmptyText,
      "sex" -> optional(text),
      "dateBorn" -> optional(date),
      "yearBorn" -> optional(javaNumber(min = 1900)),
      "fatherFirstName" -> optional(text),
      "fatherLastName" -> optional(text),
      "motherFirstName" -> optional(text),
      "motherLastName" -> optional(text),
      "fatherOccupation" -> optional(text),
      "motherOccupation" -> optional(text))(Godchild.apply)(Godchild.unapply))

  def find(nameFragment: String) = RequiresAuthentication("FormClient", "", true) { profile =>
    Action { implicit request =>
      val godchildren = godchildRepository.findByNameFragment(nameFragment.asFragmentMatch).iterator()
      asJsonResult(godchildren)
    }
  }

  def detailView(nodeId: Long) = RequiresAuthentication("FormClient") { profile =>
    Action { implicit request =>
      val godchild = godchildRepository.findByNodeId(nodeId)
      if (godchild != null) {
        Ok(views.html.godchildren.detail(godchild))
      } else {
        Redirect(routes.Godchildren.listView()).flashing("error" -> "Item not found")
      }
    }
  }

  def update(nodeId: Long) = RequiresAuthentication("FormClient", "", true) { profile =>
    Action { implicit request =>
      val updateForm = this.godchildForm.bindFromRequest()
      updateForm.fold(
        hasErrors = { form =>
          asFormBadRequest(form)
        },
        success = { godchild =>
          // Get existing profile image and set to edited godchild object
          if (!getMultipartFormData(request).file(profileImageFileParamName).isDefined) {
            val origGodchild = godchildRepository.findByNodeId(nodeId)
            godchild.setProfileImage(origGodchild.getProfileImage)
          }

          godchildRepository.save(godchild)
          saveProfileImage(godchild, getMultipartFormData(request))

          Ok(Json.obj("message" -> Messages("form.success"),
            "link" -> routes.Godchildren.detailView(godchild.getNodeId).url,
            "entity" -> Json.parse(new ObjectMapper().writeValueAsString(godchild))))
        })
    }
  }

  def editView(nodeId: Long) = RequiresAuthentication("FormClient") { profile =>
    Action { implicit request =>
      val form = if (request2flash.get("error").isDefined) {
        this.godchildForm.bind(request2flash.data)
      } else {
        val godchild = godchildRepository.findByNodeId(nodeId)
        if (godchild == null) null else this.godchildForm.fill(godchild)
      }

      if (form == null) {
        Redirect(routes.Godchildren.listView()).flashing("error" -> "Item not found")
      } else {
        Ok(views.html.godchildren.edit(form, nodeId))
      }
    }
  }

  def save = RequiresAuthentication("FormClient", "", true) { profile =>
    Action { implicit request =>
      val newGodchildForm = this.godchildForm.bindFromRequest()
      newGodchildForm.fold(
        hasErrors = { form =>
          asFormBadRequest(form)
        },
        success = { godchild =>
          godchildRepository.save(godchild)

          // Store profile image
          saveProfileImage(godchild, getMultipartFormData(request))

          Ok(Json.obj("message" -> Messages("form.success"), "link" -> routes.Godchildren.detailView(godchild.getNodeId).url))
        })
    }
  }

  def getMultipartFormData(request: Request[AnyContent]) = {
    request.body.asMultipartFormData.get
  }
  
  def saveProfileImage(godchild: Godchild, multiFormData: MultipartFormData[Files.TemporaryFile]) = {
    val profileImage = multiFormData.file(profileImageFileParamName)
    if (profileImage.isDefined) {
      godchildService.saveProfileImage(godchild, profileImage.get.ref.file)
    }
  }

  def createView = RequiresAuthentication("FormClient") { profile =>
    Action { implicit request =>
      val form = if (request2flash.get("error").isDefined) {
        this.godchildForm.bind(request2flash.data)
      } else {
        this.godchildForm
      }

      Ok(views.html.godchildren.create(form))
    }
  }

  def delete(nodeId: Long) = RequiresAuthentication("FormClient") { profile =>
    Action { implicit request =>
      godchildService.deleteGodchild(nodeId)
      Ok
    }
  }

  def flushList() = RequiresAuthentication("FormClient", "", true) { profile =>
    Action { implicit request =>
      Ok
    }
  }

  def withoutDonor = RequiresAuthentication("FormClient", "", true) { profile =>
    Action { implicit request =>
      val godchildren = godchildRepository.withoutDonor.iterator()
      asJsonResult(godchildren)
    }
  }

  def list() = RequiresAuthentication("FormClient", "", true) { profile =>
    Action { implicit request =>
      val godchildren = godchildRepository.findAll();
      asJsonResultWithEtag(godchildren, request)
    }
  }

  def listView() = RequiresAuthentication("FormClient") { profile =>
    Action { implicit request =>
      Ok(views.html.godchildren.list(routes.Godchildren.list().url))
    }
  }

  def readProfileImage(nodeId: Long) = RequiresAuthentication("FormClient") { profile =>
    val godchild = godchildRepository.findByNodeId(nodeId)
    val image = godchildService.readProfileImageAsBytes(godchild)
    val imgContent: Enumerator[Array[Byte]] = Enumerator(image)

    if (image != null) {
      Action {
        Result(
          header = ResponseHeader(200),
          body = imgContent)
      }
    } else {
      Assets.at("/public", "images/empty-profile-image.gif")
    }
  }

}