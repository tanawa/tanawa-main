package controllers

/**
 * Based on chooze from julienrf (https://github.com/julienrf/chooze).
 *
 * @author Dani
 */
import play.api.mvc.{Action, Controller}
import play.api.i18n.Lang
import play.api.data._
import play.api.data.Forms._
import play.api.Logger
import play.api.Play.current

object UserProfile extends Controller {
  val HOME_URL = "/"

  val localeForm = Form("locale" -> nonEmptyText)

  def changeLocale = Action { implicit request =>
    val referrer = request.headers.get(REFERER).getOrElse(HOME_URL)
    localeForm.bindFromRequest.fold(
      errors => {
        BadRequest("Locale could not be changed")
      },
      locale => {
        Logger.logger.debug("Change user lang to : " + locale)
        Redirect(referrer).withLang(Lang(locale))
      })
  }
}
