package controllers

import java.lang.{Integer => JInt, Long => JLong}

import play.api.data.Forms.{longNumber, number}
import play.api.data.Mapping

/**
 * Provides additional functions providing a smooth interaction between Scala and Java.
 *
 * @author Dani
 */
trait JavaHelper {
  val javaLongNumber: Mapping[JLong] = longNumber.transform(sLong => JLong.valueOf(sLong), jLong => jLong)
  val javaNumber: Mapping[JInt] = number.transform(sInt => JInt.valueOf(sInt), jInt => jInt)

  def javaNumber(min: Int = Int.MinValue, max: Int = Int.MaxValue, strict: Boolean = false): Mapping[JInt]
  = number(min = min, max = max, strict = strict).transform(sInt => JInt.valueOf(sInt), jInt => jInt)
}
