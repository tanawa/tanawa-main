package controllers

/**
 * @author Dani
 */
case class RegexString(string: String) {
  def asFragmentMatch = "(?i)" + asCaseSensitiveFragmentMatch
  def asCaseSensitiveFragmentMatch = ".*" + string + ".*"
}

trait QueryHelper {
  implicit def asRegexString(string: String) = RegexString(string)
}
