package controllers

import org.pac4j.http.client.FormClient
import org.pac4j.play.Config
import org.pac4j.play.scala.ScalaController
import play.api.Routes
import play.api.mvc.Action
import routes.javascript.Godchildren
import routes.javascript.Donors
import routes.javascript.Countries

object Application extends ScalaController {

  def login = Action { implicit request =>
    val formClient = Config.getClients().findClient("FormClient").asInstanceOf[FormClient]
    Ok(views.html.login(formClient.getCallbackUrl()))
  }

  def index = RequiresAuthentication("FormClient") { profile =>
    Action { implicit request =>
      Ok(views.html.home.index(profile))
    }
  }

  def javascriptRoutes = Action { implicit request =>
    import routes.javascript._
    Ok(
      Routes.javascriptRouter("jsRoutes")(
        Godchildren.detailView,
        Godchildren.editView,
        Godchildren.update,
        Godchildren.find,
        Godchildren.delete,
        Donors.detailView,
        Donors.update,
        Donors.find,
        Donors.delete,
        Donors.supportGodchild,
        Donors.getById,
        Donors.endSupportGodchild,
        Countries.list)).as("text/javascript")
  }

}