package controllers

import models.repository.{ GodchildRepository, DonorRepository, SupportsRelationshipRepository, AddressRepository, CityRepository }
import models.{ Donor, Address, City, Country, SupportsRelationship }
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import play.api.data.Form
import play.api.data.Forms.{ default, text, email, mapping, nonEmptyText, optional, number }
import play.api.mvc.{ Action, Controller, Flash }
import org.pac4j.play.scala.ScalaController
import org.springframework.data.neo4j.support.Neo4jTemplate
import play.api.i18n.Messages
import services.CityServiceImpl

/**
 * @author Dani
 */
@Component
class Donors extends ScalaController with JsonResult with QueryHelper with JavaHelper {

  @Autowired
  var template: Neo4jTemplate = null

  @Autowired
  var donorRepository: DonorRepository = null

  @Autowired
  var addressRepository: AddressRepository = null

  @Autowired
  var cityRepository: CityRepository = null

  @Autowired
  var supportsRepository: SupportsRelationshipRepository = null

  @Autowired
  var godchildRepository: GodchildRepository = null

  @Autowired
  var cityService: CityServiceImpl = null

  private val donorForm: Form[Donor] = Form(
    mapping(
      "nodeId" -> optional(javaLongNumber),
      "title" -> optional(text),
      "firstName" -> nonEmptyText,
      "middleName" -> optional(text),
      "lastName" -> nonEmptyText,
      "email" -> email,
      "address" -> mapping(
        "nodeId" -> optional(javaLongNumber),
        "addressStreet" -> optional(text),
        "postalCode" -> optional(text),
        "city" -> mapping(
          "nodeId" -> optional(javaLongNumber),
          "name" -> optional(text))(City.apply)(City.unapply),
        "province" -> optional(text),
        "country" -> mapping(
          "nodeId" -> optional(javaLongNumber),
          "name" -> optional(text))(Country.apply)(Country.unapply))(Address.apply)(Address.unapply),
      "notes" -> optional(text(maxLength=2000)))(Donor.apply)(Donor.unapply))

  def find(nameFragment: String) = RequiresAuthentication("FormClient", "", true) { profile =>
    Action { implicit request =>
      val donors = donorRepository.findByNameFragment(nameFragment.asFragmentMatch).iterator()
      asJsonResult(donors)
    }
  }

  def getById(nodeId: Long) = RequiresAuthentication("FormClient", "", true) { profile =>
    Action {
      val donor = donorRepository.findOne(nodeId)
      template.fetch(donor.getGodchildren)
      asJsonResult(donor)
    }
  }

  def detailView(nodeId: Long) = RequiresAuthentication("FormClient") { profile =>
    Action { implicit request =>
      val donor = donorRepository.findOne(nodeId)
      template.fetch(donor.getAddress)
      if (donor != null) {
        Ok(views.html.donors.detail(donor))
      } else {
        Redirect(routes.Donors.listView()).flashing("error" -> "Donor not found")
      }
    }
  }

  def update(nodeId: Long) = RequiresAuthentication("FormClient") { profile =>
    Action { implicit request =>
      val updateForm = this.donorForm.bindFromRequest()
      updateForm.fold(
        hasErrors = { form =>
          Redirect(routes.Donors.editView(nodeId)).flashing(Flash(form.data) + ("error" -> Messages("form.error")))
        },
        success = { donor =>
          saveAddress(donor)
          donorRepository.save(donor)
          Redirect(routes.Donors.editView(nodeId)).flashing("success" -> Messages("form.success"))
        })
    }
  }

  def saveAddress(donor: Donor) = {
    val currentAddress = donor.getAddress
    if (currentAddress != null) {
      cityService.setUniqueCity(currentAddress)
      addressRepository.save(currentAddress)
    }
  }

  def editView(nodeId: Long) = RequiresAuthentication("FormClient") { profile =>
    Action { implicit request =>
      val donor = donorRepository.findOne(nodeId)
      val form = if (request2flash.get("error").isDefined) {
        this.donorForm.bind(request2flash.data)
      } else {
        template.fetch(donor.getAddress)
        if (donor == null) null else this.donorForm.fill(donor)
      }

      if (form == null) {
        Redirect(routes.Donors.listView()).flashing("error" -> "Item not found")
      } else {
        Ok(views.html.donors.edit(form, donor))
      }
    }
  }

  def save = RequiresAuthentication("FormClient") { profile =>
    Action { implicit request =>
      val newDonorForm = this.donorForm.bindFromRequest()
      newDonorForm.fold(
        hasErrors = { form =>
          Redirect(routes.Donors.createView()).flashing(Flash(form.data) + ("error" -> Messages("form.error")))
        },
        success = { donor =>
          saveAddress(donor)
          donorRepository.save(donor)
          Redirect(routes.Donors.createView()).flashing("success" -> Messages("form.success"))
        })
    }
  }

  def createView = RequiresAuthentication("FormClient") { profile =>
    Action { implicit request =>
      val form = if (request2flash.get("error").isDefined) {
        this.donorForm.bind(request2flash.data)
      } else {
        this.donorForm
      }
      Ok(views.html.donors.create(form, new Donor()))
    }
  }

  @Transactional
  def delete(nodeId: Long) = RequiresAuthentication("FormClient") { profile =>
    Action { implicit request =>
      val donor = donorRepository.findOne(nodeId)

      // Remove address if available
      if (donor.getAddress != null) {
        addressRepository.delete(donor.getAddress.getNodeId)
      }

      donorRepository.delete(nodeId)
      Ok
    }
  }

  def listView() = RequiresAuthentication("FormClient") { profile =>
    Action { implicit request =>
      Ok(views.html.donors.list(routes.Donors.list().url))
    }
  }

  def list() = RequiresAuthentication("FormClient", "", true) { profile =>
    Action { implicit request =>
      val donors = donorRepository.findAll().iterator()
      asJsonResultWithEtag(donors, request)
    }
  }

  def flushList() = RequiresAuthentication("FormClient", "", true) { profile =>
    Action { implicit request =>
      Ok
    }
  }

  def withoutGodchild = RequiresAuthentication("FormClient", "", true) { profile =>
    Action { implicit request =>
      val donors = donorRepository.withoutGodchild.iterator()
      asJsonResult(donors)
    }
  }

  @Transactional
  def supportGodchild(donorNodeId: Long, godchildNodeId: Long) = RequiresAuthentication("FormClient") { profile =>
    Action { implicit request =>
      val alreadySupports = supportsRepository.findByDonorNodeIdAndGodchildNodeId(donorNodeId, godchildNodeId).iterator()
      if (!alreadySupports.hasNext) {
        val donor = donorRepository.findOne(donorNodeId)
        val godchild = godchildRepository.findByNodeId(godchildNodeId)
        supportsRepository.save(new SupportsRelationship(donor, godchild))
        Ok
      } else {
        Redirect(routes.Donors.listView()).flashing("error" -> "Donor already supports godchild")
      }
    }
  }

  @Transactional
  def endSupportGodchild(donorNodeId: Long, godchildNodeId: Long) = RequiresAuthentication("FormClient") { profile =>
    Action { implicit request =>
      val alreadySupports = supportsRepository.findByDonorNodeIdAndGodchildNodeId(donorNodeId, godchildNodeId).iterator()
      if (alreadySupports.hasNext) {
        val supportRelation = alreadySupports.next()
        supportsRepository.delete(supportRelation)
        Ok
      } else {
        Redirect(routes.Donors.listView()).flashing("error" -> "Donor doesn't support godchild")
      }
    }
  }

}
