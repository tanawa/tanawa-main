package controllers

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.common.base.Charsets
import com.google.common.hash.Hashing
import play.api.http.ContentTypes
import play.api.http.HeaderNames.CACHE_CONTROL
import play.api.http.HeaderNames.ETAG
import play.api.http.HeaderNames.IF_NONE_MATCH
import play.api.mvc.AnyContent
import play.api.mvc.Request
import play.api.mvc.Results
import play.api.i18n.Messages
import play.api.data.Form
import play.api.libs.json.Json
import models.Godchild
import play.api.data.FormError
import play.api.libs.json.Writes
import play.api.libs.json.JsValue
import play.api.i18n.Lang

/**
 * @author Dani
 */
trait JsonResult extends Results with ContentTypes {

  /**
   * Json writer definition for writing FormError as json string.
   */
  implicit object FormErrorWrites extends Writes[FormError] {
    override def writes(o: FormError): JsValue = Json.obj(
      "key" -> Json.toJson(o.key),
      "message" -> Json.toJson(o.message))
  }

  def asJsonResult(o: AnyRef) = {
    Ok(new ObjectMapper().writeValueAsString(o)).as(JSON)
  }

  /**
   * Returns json result with ETag and Cache-Control headers with max-age of 30 seconds.
   * If the IF_NONE_MATCH header is same as the calculated etag hash then it just returns a NOT_MODIFIED response.
   */
  def asJsonResultWithEtag(o: AnyRef, request: Request[AnyContent]) = {
    val jsonResult = new ObjectMapper().writeValueAsString(o)
    val etagHash = Hashing.sha1().hashString(jsonResult, Charsets.UTF_8).toString()
    val ifNoneMatchValue = request.headers.get(IF_NONE_MATCH).getOrElse("")

    if (ifNoneMatchValue == etagHash) {
      NotModified
    } else {
      Ok(jsonResult).withHeaders(
        CACHE_CONTROL -> "max-age=30",
        ETAG -> etagHash).as(JSON)
    }
  }

  def asFormBadRequest(form: Form[Godchild])(implicit lang: Lang) = {
    val i18nFormErrors = form.errors.map(f => new FormError(f.key, Messages(f.message)(lang)))
    BadRequest(Json.obj("message" -> Messages("form.error"), "errors" -> Json.toJson(i18nFormErrors)))
  }
}
