package controllers

import org.pac4j.play.scala.ScalaController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import models.repository.CountryRepository
import play.api.mvc.Action

/**
 * @author olimination
 */
@Component
class Countries extends ScalaController with JsonResult with QueryHelper with JavaHelper {

  @Autowired
  var countryRepository: CountryRepository = null

  def list = RequiresAuthentication("FormClient", "", true) { profile =>
    Action { implicit request =>
      val countries = countryRepository.findAll().iterator()
      asJsonResult(countries)
    }
  }

}
