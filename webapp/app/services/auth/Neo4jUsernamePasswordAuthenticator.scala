package services.auth

import org.pac4j.core.exception.CredentialsException
import org.pac4j.http.credentials.UsernamePasswordAuthenticator
import org.pac4j.http.credentials.UsernamePasswordCredentials
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import com.google.common.base.Charsets
import com.google.common.hash.Hashing

import models.User
import models.repository.UserRepository

/**
 * @author olimination
 */
@Service
class Neo4jUsernamePasswordAuthenticator extends UsernamePasswordAuthenticator {

  @Autowired
  var userRepository: UserRepository = null

  override def validate(credentials: UsernamePasswordCredentials) = {
    val sha1Pw = Hashing.sha1().hashString(credentials.getPassword, Charsets.UTF_8).toString()
    val user = userRepository.findByUsernameAndPassword(credentials.getUsername, sha1Pw)

    if (user == null) {
      throw new CredentialsException("User not found.")
    }
  }

  def getUserRepository() = {
    userRepository
  }

  def setUserRepository(userRepository: UserRepository) = {
    this.userRepository = userRepository
  }

}