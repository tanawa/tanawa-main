package services.converter;

import org.apache.commons.codec.binary.Base64;
import org.springframework.core.convert.converter.Converter;

/**
 * Converts a base64-encoded String to decoded-base64 byte array.
 * This converter is used mainly because the Spring Data Neo4j stores binary data as base64-encoded byte[] type.
 * 
 * @author olimination
 */
public class StringBase64ToByteArrayConverter implements Converter<String, byte[]> {

    @Override
    public byte[] convert(String source) {
	return Base64.decodeBase64(source);
    }
}
