package services

import models.repository.CityRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import models.Address
import models.City

/**
 * Provides business logic for the City model.
 *
 * @author olimination
 */
@Service
class CityServiceImpl {

  @Autowired
  var cityRepository: CityRepository = null

  /**
   * Sets the city for an address. If it already exists, then the already existing city will be set.
   */
  def setUniqueCity(address: Address) = {
    if (address.getCity != null) {
      var city = cityRepository.findByName(address.getCity.getName)
      if (city == null) {
        city = new City(address.getCity.getName);
      }
      address.setCity(city)
    }
  }

}