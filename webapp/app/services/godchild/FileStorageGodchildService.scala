package services.godchild

import java.io.File
import org.springframework.beans.factory.annotation.Autowired
import models.Godchild
import models.repository.GodchildRepository
import services.storage.FileStorage
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service
import scala.NotImplementedError
import java.nio.file.Files

/**
 * The FileStorageGodchildService provides common functions for Godchild entities 
 * which stores the profile image separately on the filesystem.
 */
@Service
@Profile(Array("localfilebased"))
class FileStorageGodchildService extends GodchildService {

  @Autowired
  var godchildRepository: GodchildRepository = null

  @Autowired
  var fileStorage: FileStorage = null

  def generateFileId(godchild: Godchild) = {
    godchild.getNodeId + "-profile-image.png"
  }

  def generateFileId(nodeId: Long) = {
    nodeId + "-profile-image.png"
  }

  override def saveProfileImage(godchild: Godchild, file: File) = {
    fileStorage.store(generateFileId(godchild), file, true)
  }

  override def readProfileImageAsBytes(godchild: Godchild): Array[Byte] = {
    val imgFile = fileStorage.get(generateFileId(godchild))
    if(imgFile.exists()) Files.readAllBytes(imgFile.toPath()) else null
  }

  override def deleteProfileImage(nodeId: Long) = {
    fileStorage.delete(generateFileId(nodeId))
  }

  override def deleteGodchild(nodeId: Long) = {
    godchildRepository.delete(nodeId)
    deleteProfileImage(nodeId)
  }

}