package services.godchild

import models.Godchild
import org.springframework.beans.factory.annotation.Autowired
import models.repository.GodchildRepository
import java.io.File
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service
import java.nio.file.Files
import com.fasterxml.jackson.databind.ObjectMapper

/**
 * The Neo4jGodchildService handles Godchild entities via Neo4j.
 * Also the profile images are stored directly in Neo4j.
 */
@Service
@Profile(Array("default"))
class Neo4jGodchildService extends GodchildService {

  @Autowired
  var godchildRepository: GodchildRepository = null

  override def saveProfileImage(godchild: Godchild, file: File) = {
    val data = Files.readAllBytes(file.toPath())
    godchild.setProfileImage(data)
    godchildRepository.save(godchild)
    file.delete()
  }

  override def readProfileImageAsBytes(godchild: Godchild): Array[Byte] = {
    godchild.getProfileImage
  }

  override def deleteProfileImage(nodeId: Long) = {
    val godchild = godchildRepository.findByNodeId(nodeId)
    godchild.setProfileImage(null)
  }

  override def deleteGodchild(nodeId: Long) = {
    godchildRepository.delete(nodeId)
  }

}