package services.godchild

import models.Godchild
import java.io.File

/**
 * The GodchildService trait provides a common set of methods for business logic around the
 * Godchild management.
 */
trait GodchildService {

  def saveProfileImage(godchild: Godchild, file: File)
  
  /**
   * Returns the bytes of the binary, else null.
   */
  def readProfileImageAsBytes(godchild: Godchild): Array[Byte]

  def deleteProfileImage(nodeId: Long)

  def deleteGodchild(nodeId: Long)
}