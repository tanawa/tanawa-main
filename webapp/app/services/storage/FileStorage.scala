package services.storage

import java.io.File

/**
 * The FileStorage trait defines methods for accessing files in different places.
 * <p>
 * The fileId can be freely set and will be used as the name of the stored file.
 * Make sure that the fileId is unique in the system.
 */
trait FileStorage {

  /**
   * Stores a file by the given fileId.
   * <p>
   * If you have a temporary file, e.g. after a form upload, you can set the isMove parameter to true. Then
   * this temp file will just be moved to the new defined target.
   *
   * @param fileId A unique string for the file id.
   * @param file The file to be stored.
   * @param isMove If the given file is only temporary and should be moved to the target with the fileId name of the new file.
   */
  def store(fileId: String, file: File, isMove: Boolean = false)

  def get(fileId: String): File
  
  def delete(fileId: String)
}