package services.storage

import org.springframework.stereotype.Service
import java.io.File
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import org.springframework.context.annotation.Profile

/**
 * @author olimination
 */
@Service
@Profile(Array("localfilebased"))
class LocalFileStorage extends FileStorage {

  val BASE_STORAGE_PATH = "profile-images"

  def getPath(fileId: String) = {
    FileSystems.getDefault().getPath(BASE_STORAGE_PATH, fileId)
  }
  
  /**
   * Stores a file with the given fileId.
   * <p>
   * It deletes first the file if it exists. This method then also can be used for updates.
   */
  override def store(fileId: String, file: File, isMove: Boolean = false) {
    val target = getPath(fileId)
    Files.deleteIfExists(target)
    
    if(isMove) {
      Files.createDirectories(target)
      Files.move(file.toPath(), target, StandardCopyOption.REPLACE_EXISTING)
    } else {
      Files.createDirectories(target)
      Files.createFile(target)
    }
  }
  
  override def get(fileId: String): File = {
    getPath(fileId).toFile
  }
  
  override def delete(fileId: String) = {
    Files.deleteIfExists(getPath(fileId))
  }
}
