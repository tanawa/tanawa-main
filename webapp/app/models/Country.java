package models;

import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.NodeEntity;

import scala.Option;
import scala.Some;
import scala.Tuple2;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The countries are provided statically. Therefore we only need to manage the
 * appropriate nodeId of the country and don't have to handle the name property
 * in forms.
 * 
 * @author olimination
 * @since 20141230
 */
@NodeEntity
public class Country {

    @GraphId
    private Long nodeId;

    private String name;

    public Country() {
    }

    public Country(Option<Long> nodeId, Option<String> name) {
	if (nodeId.isDefined()) {
	    this.nodeId = nodeId.get();
	}
	if (name.isDefined()) {
	    this.name = name.get();
	}
    }

    public static Country apply(Option<Long> nodeId, Option<String> name) {
	return new Country(nodeId, name);
    }

    public static Some<Tuple2<Option<Long>, Option<String>>> unapply(Country c) {
	return c == null ? new Some<>(new Tuple2<>(scala.Option.apply(-1L), scala.Option.apply(""))) : new Some<>(new Tuple2<>(
		c.getNodeIdOption(), c.getNameOption()));
    }

    public Long getNodeId() {
	return nodeId;
    }

    public String getName() {
	return name;
    }

    @JsonIgnore
    public Option<Long> getNodeIdOption() {
	return scala.Option.apply(nodeId);
    }

    @JsonIgnore
    public Option<String> getNameOption() {
	return scala.Option.apply(name);
    }

    public boolean isDefined() {
	return nodeId != null;
    }
}
