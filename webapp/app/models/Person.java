package models;

import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

import scala.Option;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The Person implements the Comparable interface to guarantee the order of the
 * Set items. This is mainly used for the ETag generation for HTTP Caching.
 * 
 * @author dani
 */
@NodeEntity
public class Person implements Comparable<Person> {
    @GraphId
    private Long nodeId;
    @Indexed
    private String firstName;
    @Indexed
    private String middleName;
    @Indexed
    private String lastName;

    private String sex;

    @RelatedTo(type = "HAS_ADDRESS")
    private Address address;

    public Person() {
    }

    public Person(Option<Long> nodeId, String firstName, Option<String> middleName, String lastName, Option<String> sex, Address address) {
	if (nodeId.isDefined()) {
	    this.nodeId = nodeId.get();
	}
	this.firstName = firstName;
	if (middleName.isDefined()) {
	    this.middleName = middleName.get();
	}
	this.lastName = lastName;

	if (address != null && address.isDefined()) {
	    this.address = address;
	}

	if (sex != null && sex.isDefined()) {
	    this.sex = sex.get();
	}
    }

    public Person(String firstName, String lastName) {
	this.firstName = firstName;
	this.lastName = lastName;
    }

    public String getFirstName() {
	return firstName;
    }

    public String getLastName() {
	return lastName;
    }

    public Long getNodeId() {
	return nodeId;
    }

    public String getMiddleName() {
	return middleName;
    }

    public String getSex() {
	return sex;
    }

    public Address getAddress() {
	return address;
    }

    @JsonIgnore
    public Option<String> getMiddleNameOption() {
	return scala.Option.apply(middleName);
    }

    @JsonIgnore
    public Option<Long> getNodeIdOption() {
	return scala.Option.apply(nodeId);
    }

    @JsonIgnore
    public Option<String> getSexOption() {
	return scala.Option.apply(sex);
    }

    /**
     * Implements just a nodeId comparator for guaranteeing the same order in
     * Set items. The Set order needs to be always the same so that an unique
     * ETag can be generated for the HTTP header. Example: Donors have a Set of
     * godchildren.
     */
    @Override
    public int compareTo(Person o) {
	return this.getNodeId().compareTo(o.getNodeId());
    }
}
