package models;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

/**
 * @author Dani
 */
@RelationshipEntity(type = "SUPPORTS")
public class SupportsRelationship {

    @GraphId
    private Long id;

    @StartNode
    private Donor donor;

    @EndNode
    private Godchild godchild;

    public SupportsRelationship() {}

    public SupportsRelationship(Donor donor, Godchild godchild) {
        this.donor = donor;
        this.godchild = godchild;
    }
}
