package models;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

import scala.Option;
import scala.Some;
import scala.Tuple6;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The {@link Address} entity models the address of a person. The address is
 * optional. If no address data is provided then the system should also not
 * create any empty entities.
 * 
 * @author olimination
 * @since 20141230
 */
@NodeEntity
public class Address {

    @GraphId
    private Long nodeId;

    /**
     * Defines the address in free form: street name, number, Lot, block,
     * building, etc.
     */
    private String addressStreet;

    private String postalCode;

    @RelatedTo(type = "HAS_CITY")
    @Fetch
    private City city;

    private String province;

    @RelatedTo(type = "HAS_COUNTRY")
    @Fetch
    private Country country;

    public Address() {
    }

    public Address(Option<Long> nodeId, Option<String> addressStreet, Option<String> postalCode, City city, Option<String> province,
	    Country country) {
	if (nodeId.isDefined()) {
	    this.nodeId = nodeId.get();
	}
	if (addressStreet.isDefined()) {
	    this.addressStreet = addressStreet.get();
	}
	if (postalCode.isDefined()) {
	    this.postalCode = postalCode.get();
	}
	if (city.isDefined()) {
	    this.city = city;
	}
	if (province.isDefined()) {
	    this.province = province.get();
	}
	if (country.isDefined()) {
	    this.country = country;
	}
    }

    public static Address apply(Option<Long> nodeId, Option<String> addressStreet, Option<String> postalCode, City city,
	    Option<String> province, Country country) {
	return new Address(nodeId, addressStreet, postalCode, city, province, country);
    }

    public static Some<Tuple6<Option<Long>, Option<String>, Option<String>, City, Option<String>, Country>> unapply(Address adr) {
	return adr == null ? new Some<>(new Tuple6<>(scala.Option.apply(-1L), scala.Option.apply(""), scala.Option.apply(""), new City(),
		scala.Option.apply(""), new Country())) : new Some<>(new Tuple6<>(adr.getNodeIdOption(), adr.getAddressStreetOption(),
		adr.getPostalCodeOption(), adr.getCity(), adr.getProvinceOption(), adr.getCountry()));
    }

    public Long getNodeId() {
	return nodeId;
    }

    @JsonIgnore
    public Option<Long> getNodeIdOption() {
	return scala.Option.apply(nodeId);
    }

    public String getAddressStreet() {
	return addressStreet;
    }

    @JsonIgnore
    public Option<String> getAddressStreetOption() {
	return scala.Option.apply(addressStreet);
    }

    public String getPostalCode() {
	return postalCode;
    }

    @JsonIgnore
    public Option<String> getPostalCodeOption() {
	return scala.Option.apply(postalCode);
    }

    public City getCity() {
	return city;
    }

    public void setCity(City city) {
	this.city = city;
    }

    public String getProvince() {
	return province;
    }

    @JsonIgnore
    public Option<String> getProvinceOption() {
	return scala.Option.apply(province);
    }

    public Country getCountry() {
	return country;
    }

    public boolean isDefined() {
	return isNotEmpty(addressStreet) || isNotEmpty(postalCode) || city != null || isNotEmpty(province) || country != null;
    }
}
