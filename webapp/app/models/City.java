package models;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.NodeEntity;

import scala.Option;
import scala.Some;
import scala.Tuple2;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A City entity should only exist once in the system.
 * Therefore we check for every save/update if the name of the city already exists.
 * 
 * @author olimination
 * @since 20141230
 */
@NodeEntity
public class City {

    @GraphId
    private Long nodeId;

    private String name;

    public City() {
    }
    
    public City(String name) {
	this.name = name;
    }

    public City(Option<Long> nodeId, Option<String> cityName) {
	if (nodeId.isDefined()) {
	    this.nodeId = nodeId.get();
	}
	if (cityName.isDefined()) {
	    this.name = cityName.get();
	}
    }

    public static City apply(Option<Long> nodeId, Option<String> cityName) {
	return new City(nodeId, cityName);
    }

    public static Some<Tuple2<Option<Long>, Option<String>>> unapply(City c) {
	return c == null ? new Some<>(new Tuple2<>(scala.Option.apply(-1L), scala.Option.apply(""))) : new Some<>(new Tuple2<>(
		c.getNodeIdOption(), c.getNameOption()));

    }

    public Long getNodeId() {
	return nodeId;
    }

    @JsonIgnore
    public Option<Long> getNodeIdOption() {
	return scala.Option.apply(nodeId);
    }

    public String getName() {
	return name;
    }

    @JsonIgnore
    public Option<String> getNameOption() {
	return scala.Option.apply(name);
    }
    
    public boolean isDefined() {
	return isNotEmpty(name);
    }
}
