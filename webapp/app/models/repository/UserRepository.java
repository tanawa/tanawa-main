package models.repository;

import models.Godchild;
import models.User;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * @author olimination
 */
public interface UserRepository extends GraphRepository<Godchild> {
    @Query("MATCH (u:User) WHERE u.username = {0} AND u.password = {1} RETURN u")
    User findByUsernameAndPassword(String username, String password);
}
