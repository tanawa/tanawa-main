package models.repository;

import models.City;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.conversion.Result;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * @author olimination
 */
public interface CityRepository extends GraphRepository<City> {
    
    City findByName(String name);
    
    @Query("MATCH (c:City) RETURN c ORDER BY c.name ASC")
    Result<City> findAll();
}
