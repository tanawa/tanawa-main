package models.repository;

import models.SupportsRelationship;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * @author Dani
 */
public interface SupportsRelationshipRepository extends GraphRepository<SupportsRelationship> {
    @Query("match (p:Donor)-[s:SUPPORTS]->(c:Godchild) WHERE ID(p) = {0} and ID(c) = {1} return s")
    Iterable<SupportsRelationship> findByDonorNodeIdAndGodchildNodeId(Long donorNodeId, Long godchildNodeId);
}
