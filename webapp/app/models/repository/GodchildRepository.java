package models.repository;

import models.Godchild;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.conversion.Result;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * @author dani
 */
public interface GodchildRepository extends GraphRepository<Godchild> {
    
    Godchild findByNodeId(Long nodeId);
    
    @Query("MATCH (gc:Godchild) RETURN gc ORDER BY gc.firstName ASC")
    Result<Godchild> findAll();
    
    @Query("MATCH (c:Godchild) WHERE c.firstName =~ {0} OR c.middleName =~ {0} OR c.lastName =~ {0} RETURN c")
    Iterable<Godchild> findByNameFragment(String nameFragment);

    @Query("MATCH (c:Godchild) WHERE NOT (c)<-[:SUPPORTS]-() RETURN c")
    Iterable<Godchild> withoutDonor();
}
