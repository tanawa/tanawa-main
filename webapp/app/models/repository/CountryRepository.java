package models.repository;

import models.Country;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.conversion.Result;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * @author olimination
 */
public interface CountryRepository extends GraphRepository<Country> {
    
    @Query("MATCH (c:Country) RETURN c ORDER BY c.name ASC")
    Result<Country> findAll();
}
