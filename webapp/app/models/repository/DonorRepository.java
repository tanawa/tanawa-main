package models.repository;

import models.Donor;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.conversion.Result;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * @author Dani
 */
public interface DonorRepository extends GraphRepository<Donor> {
    
    @Query("MATCH (gp:Donor) RETURN gp ORDER BY gp.firstName ASC")
    Result<Donor> findAll();
    
    @Query("match(p:Donor) where p.firstName =~ {0} or p.middleName =~ {0} or p.lastName =~ {0} return p")
    Iterable<Donor> findByNameFragment(String nameFragment);

    @Query("MATCH (p:Donor) WHERE NOT (p)-[:SUPPORTS]->() RETURN p")
    Iterable<Donor> withoutGodchild();
}
