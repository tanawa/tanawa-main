package models.repository;

import models.Address;

import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * @author olimination
 */
public interface AddressRepository extends GraphRepository<Address> {
}
