package models;

import java.util.Date;

import org.springframework.data.neo4j.annotation.GraphProperty;

import scala.Option;
import scala.Some;
import scala.Tuple13;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dani
 */
public class Godchild extends Person {
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "nodeId")
    private Date dateBorn;
    private int yearBorn;
    private String fatherFirstName;
    private String fatherLastName;
    private String motherFirstName;
    private String motherLastName;
    private String fatherOccupation;
    private String motherOccupation;

    @JsonIgnore
    @GraphProperty(propertyType = byte[].class)
    private byte[] profileImage;

    public Godchild() {
	super();
    }

    public Godchild(Option<Long> nodeId, String firstName, Option<String> middleName, String lastName, Option<String> sex,
	    Option<Date> dateBorn, Option<Integer> yearBorn, Option<String> fatherFirstName, Option<String> fatherLastName,
	    Option<String> motherFirstName, Option<String> motherLastName, Option<String> fatherOccupation, Option<String> motherOccupation) {
	super(nodeId, firstName, middleName, lastName, sex, null);
	if (dateBorn.isDefined()) {
	    this.dateBorn = dateBorn.get();
	}
	if (yearBorn.isDefined()) {
	    this.yearBorn = yearBorn.get();
	}
	if (fatherFirstName.isDefined()) {
	    this.fatherFirstName = fatherFirstName.get();
	}
	if (fatherLastName.isDefined()) {
	    this.fatherLastName = fatherLastName.get();
	}
	if (motherFirstName.isDefined()) {
	    this.motherFirstName = motherFirstName.get();
	}
	if (motherLastName.isDefined()) {
	    this.motherLastName = motherLastName.get();
	}
	if (fatherOccupation.isDefined()) {
	    this.fatherOccupation = fatherOccupation.get();
	}
	if (motherOccupation.isDefined()) {
	    this.motherOccupation = motherOccupation.get();
	}
    }

    public Date getDateBorn() {
	return dateBorn;
    }

    public int getYearBorn() {
	return yearBorn;
    }

    @JsonIgnore
    public Option<Date> getDateBornOption() {
	return scala.Option.apply(dateBorn);
    }

    @JsonIgnore
    public Option<Integer> getYearBornOption() {
	return scala.Option.apply(yearBorn);
    }

    public String getFatherFirstName() {
	return fatherFirstName;
    }

    public String getFatherLastName() {
	return fatherLastName;
    }

    public String getMotherFirstName() {
	return motherFirstName;
    }

    public String getMotherLastName() {
	return motherLastName;
    }

    public String getFatherOccupation() {
	return fatherOccupation;
    }

    public String getMotherOccupation() {
	return motherOccupation;
    }

    @JsonIgnore
    public Option<String> getFatherFirstNameOption() {
	return scala.Option.apply(fatherFirstName);
    }

    @JsonIgnore
    public Option<String> getFatherLastNameOption() {
	return scala.Option.apply(fatherLastName);
    }

    @JsonIgnore
    public Option<String> getMotherFirstNameOption() {
	return scala.Option.apply(motherFirstName);
    }

    @JsonIgnore
    public Option<String> getMotherLastNameOption() {
	return scala.Option.apply(motherLastName);
    }

    @JsonIgnore
    public Option<String> getFatherOccupationOption() {
	return scala.Option.apply(fatherOccupation);
    }

    @JsonIgnore
    public Option<String> getMotherOccupationOption() {
	return scala.Option.apply(motherOccupation);
    }

    public byte[] getProfileImage() {
	return profileImage;
    }

    public void setProfileImage(byte[] profileImage) {
	this.profileImage = profileImage;
    }

    public static Godchild apply(Option<Long> nodeId, String firstName, Option<String> middleName, String lastName, Option<String> sex,
	    Option<Date> dateBorn, Option<Integer> yearBorn, Option<String> fatherFirstName, Option<String> fatherLastName,
	    Option<String> motherFirstName, Option<String> motherLastName, Option<String> fatherOccupation, Option<String> motherOccupation) {
	return new Godchild(nodeId, firstName, middleName, lastName, sex, dateBorn, yearBorn, fatherFirstName, fatherLastName,
		motherFirstName, motherLastName, fatherOccupation, motherOccupation);
    }

    public static Some<Tuple13<Option<Long>, String, Option<String>, String, Option<String>, Option<Date>, Option<Integer>, Option<String>, Option<String>, Option<String>, Option<String>, Option<String>, Option<String>>> unapply(
	    Godchild g) {
	return new Some<>(new Tuple13<>(g.getNodeIdOption(), g.getFirstName(), g.getMiddleNameOption(), g.getLastName(), g.getSexOption(),
		g.getDateBornOption(), g.getYearBornOption(), g.getFatherFirstNameOption(), g.getFatherLastNameOption(),
		g.getMotherFirstNameOption(), g.getMotherLastNameOption(), g.getFatherOccupationOption(), g.getMotherOccupationOption()));
    }
}
