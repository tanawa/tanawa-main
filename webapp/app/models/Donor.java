package models;

import java.util.Set;
import java.util.TreeSet;

import org.springframework.data.neo4j.annotation.RelatedTo;

import scala.Option;
import scala.Some;
import scala.Tuple8;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author Dani
 */
public class Donor extends Person {
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "nodeId")
    private String title;
    private String email;
    private String notes;

    /**
     * Using TreeSet to define a constant order for the items because of HTTP
     * ETag header generation.
     */
    @RelatedTo(type = "SUPPORTS")
    private TreeSet<Godchild> godchildren;

    public Donor() {
	super();
    }

    public Donor(Option<Long> nodeId, Option<String> title, String firstName, Option<String> middleName, String lastName, String email,
	    Address address, Option<String> notes) {
	super(nodeId, firstName, middleName, lastName, null, address);
	if (title.isDefined()) {
	    this.title = title.get();
	}
	if (notes.isDefined()) {
	    this.notes = notes.get();
	}
	this.email = email;
    }

    public String getEmail() {
	return email;
    }

    public Set<Godchild> getGodchildren() {
	return godchildren;
    }

    public String getTitle() {
	return title;
    }

    @JsonIgnore
    public Option<String> getTitleOption() {
	return scala.Option.apply(title);
    }

    public String getNotes() {
	return notes;
    }

    @JsonIgnore
    public Option<String> getNotesOption() {
	return scala.Option.apply(notes);
    }

    public static Donor apply(Option<Long> nodeId, Option<String> title, String firstName, Option<String> middleName, String lastName,
	    String email, Address address, Option<String> notes) {
	return new Donor(nodeId, title, firstName, middleName, lastName, email, address, notes);
    }

    public static Some<Tuple8<Option<Long>, Option<String>, String, Option<String>, String, String, Address, Option<String>>> unapply(
	    Donor g) {
	return new Some<>(new Tuple8<>(g.getNodeIdOption(), g.getTitleOption(), g.getFirstName(), g.getMiddleNameOption(), g.getLastName(),
		g.getEmail(), g.getAddress(), g.getNotesOption()));
    }
}
