package models;

import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;

/**
 * @author olimination
 */
@NodeEntity
public class User {

	@GraphId
	private Long nodeId;

	/**
	 * Username is email of user.
	 */
	@Indexed
	private String username;

	private String password;

	public User() {
	}

	public Long getNodeId() {
		return nodeId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
