import org.pac4j.core.client.Clients
import org.pac4j.http.client.FormClient
import org.pac4j.play.Config
import org.springframework.context.ApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext
import org.springframework.stereotype.Service
import play.api.Application
import play.api.GlobalSettings
import play.api.Play.current
import services.auth.Neo4jUsernamePasswordAuthenticator

/**
 * @author Dani
 */
object Global extends GlobalSettings {

  var applicationContext: ApplicationContext = null

  override def onStart(app: Application) {
    applicationContext = new ClassPathXmlApplicationContext("context.xml")

    val baseUrl = current.configuration.getString("baseUrl").get

    // Form Auth
    val formClient = new FormClient(baseUrl + "/login", applicationContext.getBean(classOf[Neo4jUsernamePasswordAuthenticator]))
    val clients = new Clients(baseUrl + "/callback", formClient)
    Config.setClients(clients)

  }

  override def getControllerInstance[A](controllerClass: Class[A]): A = {
    applicationContext.getBean(controllerClass)
  }

}
