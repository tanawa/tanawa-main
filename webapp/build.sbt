import com.typesafe.sbt.SbtAspectj._
import com.typesafe.sbt.SbtAspectj.AspectjKeys._
import com.typesafe.config._

val conf = ConfigFactory.parseFile(new File("conf/application.conf")).resolve()

name := """org.tanawa.webapp"""

version := conf.getString("app.version")

scalaVersion := "2.11.4"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

resolvers ++= Seq(
  "spring" at "http://repo.spring.io/milestone",
  "neo4j-releases" at "http://m2.neo4j.org/releases/"
)

libraryDependencies ++= Seq(
  "com.typesafe.play"        % "play-cache_2.11"           % "2.3.3",
  "org.springframework"      % "spring-test"               % "4.1.4.RELEASE" % "compile",
  "org.springframework.data" % "spring-data-neo4j"         % "3.2.2.RELEASE" % "compile",
  "org.springframework.data" % "spring-data-neo4j-rest"    % "3.2.2.RELEASE" % "compile",
  "com.fasterxml.jackson.core" % "jackson-core"            % "2.5.0",
  "com.fasterxml.jackson.core" % "jackson-databind"        % "2.5.0",
  "com.fasterxml.jackson.core" % "jackson-annotations"     % "2.5.0",
  "javax.persistence"        % "persistence-api"           % "1.0"           % "compile",
  "javax.validation"         % "validation-api"            % "1.0.0.GA"      % "compile",
  "javax.ws.rs"              % "jsr311-api"                % "1.1.1"         % "compile",
  "com.sun.jersey"           % "jersey-core"               % "1.18.1"        % "compile",
  "org.pac4j"                % "play-pac4j_scala2.11"      % "1.3.0",
  "org.pac4j"                % "pac4j-http"                % "1.6.0",
  "com.google.guava"         % "guava"                     % "18.0",
  "junit"                    % "junit"                     % "4.11"          % "test",
  "com.novocode"             % "junit-interface"           % "0.9"           % "test",
  "org.scalatest"            % "scalatest_2.11"            % "2.2.1"         % "test",
  "org.scalatestplus"        % "play_2.11"                 % "1.2.0"         % "test"
)


Seq(aspectjSettings: _*)

verbose in Aspectj := true

showWeaveInfo in Aspectj := true

inputs in Aspectj <+= compiledClasses

binaries in Aspectj <++= update map { report:UpdateReport =>
  report.matching(
    moduleFilter(organization = "org.springframework.data", name = "spring-data-neo4j-aspects")
  )
}

products in Compile <<= products in Aspectj

products in Runtime <<= products in Compile

