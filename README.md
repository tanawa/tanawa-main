# Tanawa - The open source donorship management tool 
Tanawa is a kind of ERP/CRM system to easily manage donors and children. 
It's modular architecture allows easily to extend the system to further needs of an organization.
It is based on the latest technologies.

## Website:

http://tanawa.bitbucket.org/

  - Docs source stored in _docs_ folder
  - Uses Hugo site generator: http://hugo.spf13.com/
  - Diagrams: draw.io

## Technology stack:
  - AngularJS, Bootstrap, jQuery, NPM, Browserify, GulpJS
  - Play 2.x with Scala 2.11
  - Spring Data, Neo4j

## Developers

See [Docs](http://tanawa.bitbucket.org/page/docs/)

## Issue Tracking

See [Jira](https://tanawa.atlassian.net/secure/Dashboard.jspa)

## License
Tanawa is released under version 2.0 of the [Apache License][]

[Apache License]: http://www.apache.org/licenses/LICENSE-2.0