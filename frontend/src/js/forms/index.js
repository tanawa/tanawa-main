var Forms = {
		
	/**
	 * FormMessages provides functions for displaying form messages uniquely.
	 */
	FormMessages: {
		displaySuccess: function(formId, data) {
			// Cleanup messages
        	jQuery("div.alert-danger").hide();
        	jQuery("div.form-group").removeClass("has-error");
        	jQuery("span.help-block.errors").hide();
        	jQuery("#" + formId).each(function() {
        		this.reset();
        	});
        	
        	// Set link to item
        	if(data.link) {
        		jQuery("a.alert-success.item-link").attr("href", data.link);
        	}
        	
        	jQuery("div.alert-success span.message-text").text(data.message);
        	jQuery("div.alert-success").show();
		},
		
		displayError: function(data) {
        	// Cleanup older messages
        	jQuery("div.alert-success").hide();
        	
        	// Display error messages
        	jQuery("div.form-group").removeClass("has-error");
        	jQuery("span.help-block.errors").hide();
        	
    		angular.forEach(data.errors, function(value, key) {
    			jQuery("span.help-block.errors." + value.key).parent("div.form-group").addClass("has-error");
    			jQuery("span.help-block.errors." + value.key).text(value.message).show();
    		});
        	
        	jQuery("div.alert-danger").show();
		}
	}
};

module.exports = Forms;