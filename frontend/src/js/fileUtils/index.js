var FileUtils = {
		
	/**
	 * Converts a given data uri string to a blob object.
	 */
	dataURItoBlob : function(dataUri, mime) {
		// convert base64 to raw binary data held in a string
		// doesn't handle URLEncoded DataURIs
		var byteString = window.atob(dataUri.split(',')[1]);

		// write the bytes of the string to an ArrayBuffer
		// var ab = new ArrayBuffer(byteString.length);
		var ia = new Uint8Array(byteString.length);
		for (var i = 0; i < byteString.length; i++) {
			ia[i] = byteString.charCodeAt(i);
		}

		// write the ArrayBuffer to a blob, and you're done
		var blob = new Blob([ ia ], {
			type : mime
		});

		return blob;
	}
};

module.exports = FileUtils;