/* jslint node: true */
/* global window, screen */
'use strict';

var appName = 'org.tanawa.app';

// Requires
var $ = require('jquery');
global.jQuery = require("jquery");
var angular = require('angular');
require('bootstrap');
require('angular-sanitize');
// A customized ui-select code because of issue: https://github.com/angular-ui/ui-select/pull/503
require('angular-ui-select');
require('ui-bootstrap');
require('metismenu');

// Tanawa modules
var countryModule = require('./country');
var godchildModule = require('./godchild');
var donorModule = require('./donor');
var imageCropInputModule = require('./imageCropInput');

// App init
var app = angular.module(appName, [ 'ui.bootstrap', 'ui.select', 'ngSanitize', countryModule.name, godchildModule.name, donorModule.name, imageCropInputModule.name ]);
app.constant('VERSION', require('../../package.json').version);

// Init leftnavigation menu accordion
$('#side-menu').metisMenu();

// Set min-height for page-wrapper element
var topOffset = 50;
var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
height = height - topOffset;
if (height < 1) height = 1;
if (height > topOffset) {
    $("#page-wrapper").css("min-height", (height) + "px");
}