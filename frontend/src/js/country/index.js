/**
 * The Country module offers the country list.
 * 
 * @author oliver.burkhalter
 * @date 20150109
 */
/*jslint node: true */
'use strict';

var appModule = require('angular').module('org.tanawa.country', []);
appModule.service('CountryService', require('./CountryService'));

module.exports = appModule;