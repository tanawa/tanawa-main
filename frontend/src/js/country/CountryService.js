/*jslint node: true */
/*global jsRoutes */
'use strict';

module.exports = function($http) {

	/**
	 * Gets the country list by http request.
	 */
	this.getCountries = function() {
		return $http.get(jsRoutes.controllers.Countries.list().url);
	};
};