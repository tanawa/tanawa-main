/**
 * The imageCropInputFile module provides a file input with image cropping.
 * 
 * @author oliver.burkhalter
 * @date 20150120
 */
/* jslint node: true */
'use strict';

require('jcrop');

var appModule = require('angular').module('org.tanawa.imageCropInput', []);

appModule.directive("tnwaImageCropInput", function($compile, $interpolate) {
	return {
		restrict : "A",
	    scope: {
	    	defaultImageUrl: '@',
	    	destImageWidth: '@',
	    	destImageHeight: '@',
	    	cropBoxWidth: '@',
	    	maxFilesize: '@'
	    },
		template: '<img id="cropPreviewImage" style="display: none;" /><img id="defaultProfileImage" /><br /><input id="uploadImageFile" name="uploadImageFile" type="file"><button id="cropButton" type="button" ng-click="finishCrop()" style="display: none;">Crop</button><div class="imageCropArea"></div>',
	    controller: ['$scope', '$http', function($scope, $http) {
	        
	    	$scope.onFileSelectChange = function(event) {
	        	if (window.File && window.FileReader && window.FileList && window.Blob) {
	                var files = event.target.files;
	                var imgFile = files[0];
	                
	                // Validation:
	                // - max image filesize, default 3.2MB
	                // - only image types
	                if(imgFile.size > $scope.maxFilesize * 1024) {
	                	alert("Image file size is too big!");
	                	jQuery("div.imageCropArea").empty();
	                } else if (!imgFile.type.match('image.*')) {
                    	alert("No image selected, please select an image!");
                    	jQuery("div.imageCropArea").empty();
	                } else {
	                    var reader = new FileReader();
	                    reader.onload = (function (tFile) {
	                        return $scope.initCropArea;
	                    } (imgFile));
	                    reader.readAsDataURL(imgFile);
	                }
	            } else {
	                alert('The File APIs are not fully supported in this browser.');
	            }
	        };
	        
	        $scope.initCropArea = function(evt) {
	        	
	        	jQuery("div.imageCropArea").html('<img id="cropImage" />');
	        	
                var origImage = new Image();
                origImage.src = evt.target.result;
                origImage.onload = function() {
                    
                    // Load image to element
                    jQuery("#cropImage").attr('src', origImage.src);
                    
                    // Init Jcrop
                    jQuery('#cropImage').Jcrop({
                    	setSelect:   [ 300, 300, 600, 600 ],
                        aspectRatio: $scope.aspectRatio,
                        boxWidth: $scope.cropBoxWidth,
                        onSelect: function(coords) { 
                            var cropCanvas = document.createElement("canvas");
                            cropCanvas.width = $scope.destImageWidth;
                            cropCanvas.height = $scope.destImageHeight;
                            jQuery(cropCanvas).attr('id', "cropCanvas");
                        	var ctx = cropCanvas.getContext('2d');
                            
                        	// Output from original image to cropped image to canvas
                        	ctx.drawImage(origImage, coords.x, coords.y, coords.w, coords.h, 0, 0,  $scope.destImageWidth, $scope.destImageHeight);
                        	
                        	// Prepare preview image
                        	jQuery("#cropPreviewImage").attr('style', 'width: ' + $scope.cropBoxWidth + 'px');
                        	jQuery("#cropPreviewImage").attr('src', cropCanvas.toDataURL("image/png"));
                        	
                        	// Cleanup
                        	jQuery("#cropCanvas").remove();
                        	ctx = null;
                        }
                    }, function() {
                    	$scope.jCropInstance = this;
                    });
                    
                    // Hide default image
                    jQuery("#defaultProfileImage").hide();
                    
                    jQuery("#cropPreviewImage").show();
                    jQuery("#cropButton").show();
                };
                
                $scope.finishCrop = function() {
                	$scope.jCropInstance.destroy();
                	jQuery("#cropImage").hide();
                	jQuery("#cropButton").hide();
                };
                
	        };
	    }],
	    link: function(scope, element, attrs, ctrl) {
	    	scope.destImageWidth = attrs.destImageWidth ? attrs.destImageWidth : 800;
	    	scope.destImageHeight = attrs.destImageHeight ? attrs.destImageHeight : 933;
	    	scope.cropBoxWidth = attrs.cropBoxWidth ? attrs.cropBoxWidth : 400;
	    	scope.aspectRatio = scope.destImageWidth / scope.destImageHeight;
	    	scope.maxFilesize = attrs.maxFilesize ? attrs.maxFilesize : 3200;
	    	document.getElementById('uploadImageFile').addEventListener('change', scope.onFileSelectChange, false);
	    	
	    	// Load default image
	    	jQuery("#defaultProfileImage").attr('style', 'width: ' + scope.cropBoxWidth + 'px');
	    	jQuery("#defaultProfileImage").attr('src', attrs.defaultImageUrl);
	    }
	}
});

module.exports = appModule;