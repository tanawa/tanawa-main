/**
 * The Donor module manages all the donor entities.
 * 
 * @author oliver.burkhalter
 * @date 20141109
 */
/*jslint node: true */
'use strict';

var appModule = require('angular').module('org.tanawa.donor', []);
appModule.controller('DonorController', ['$scope', '$http', require('./DonorController')]);
appModule.controller('DonorFormController', ['CountryService', require('./DonorFormController')]);

module.exports = appModule;