/*jslint node: true */
/*global jsRoutes, jQuery */
'use strict';

module.exports = function($scope, $http) {
	var ctrl = this;
	ctrl.list = [];
	ctrl.godchildrenWithoutDonorList = [];
	ctrl.donor = null;
	ctrl.donorIdForDeletion = null;
	ctrl.isLoading = false;
	ctrl.godchild = {
		selected: ""
	};
	ctrl.urls = {
		defaultList: "",
		godchildrenWithoutDonor: ""	
	};
	
	this.detailPath = function(nodeId) {
		return jsRoutes.controllers.Donors.detailView(nodeId).url;
	};
	
	this.editPath = function(nodeId) {
		return jsRoutes.controllers.Donors.editView(nodeId).url;
	};
	
	this.deletePath = function(nodeId) {
		return jsRoutes.controllers.Donors.delete(nodeId).url;
	};

	this.endSupport = function(godchildId) {
		var donorId = ctrl.donor.nodeId;
		
		$http.get(jsRoutes.controllers.Donors.endSupportGodchild(donorId, godchildId).url)
		.success(function(data) {
			// Reload donor data
			ctrl.getById(donorId);
			
			// Reload list
			ctrl.initGodchildrenWithoutDonorList(ctrl.urls.godchildrenWithoutDonor);
		})
		.error(function(data) {
		});
	};
	
	this.supportGodchild = function() {
		var donorId = ctrl.donor.nodeId;
		var godchildId = ctrl.godchild.selected.nodeId;
		
		if (donorId !== undefined && godchildId !== undefined) {
			$http.get(jsRoutes.controllers.Donors.supportGodchild(donorId, godchildId).url)
			.success(function(data) {
				// Add to current godchildren list of current donor
				ctrl.donor.godchildren.push(ctrl.godchild.selected);
				
				// Reset select field
				ctrl.godchild.selected = "";
				
				// Reload list
				ctrl.initGodchildrenWithoutDonorList(ctrl.urls.godchildrenWithoutDonor);
			})
			.error(function(data) {
			});	
		}
	};
	
	this.searchInList = function(query, pathToList) {
		if (!query.length) {
			this.getList(pathToList);
		} else if (query.length > 2) {
			ctrl.isLoading = true;
			this.getList(jsRoutes.controllers.Donors.find(query).url);
		}
	};

	/**
	 * Loads the appropriate list with the given url/path.
	 * This is used in the list view for donors.
	 */
	this.getList = function(path) {
		ctrl.isLoading = true;
		$http.get(path).success(function(data) {
			ctrl.list = data;
			ctrl.isLoading = false;
		}).error(function() {
			ctrl.list = [];
			ctrl.isLoading = false;
		});
	};

    this.resetList = function(path) {
        $scope.query = "";
        this.getList(path);
    };

	this.getById = function(nodeId) {
		$http.get(jsRoutes.controllers.Donors.getById(nodeId).url)
			.success(function(data) {
				ctrl.donor = data;
			})
			.error(function(data) {
				ctrl.donor = null;
			});
	};
	
	/**
	 * This list contains godchildren without donors.
	 * This is mainly used in the detail view of the donor.
	 */
	this.initGodchildrenWithoutDonorList = function(url) {
		ctrl.urls.godchildrenWithoutDonor = url;
		$http.get(url)
		.success(function(data) {
			ctrl.godchildrenWithoutDonorList = data;
		})
		.error(function(data) {
			ctrl.godchildrenWithoutDonorList = [];
		});
	};

	this.initDefaultList = function(url) {
		ctrl.urls.defaultList = url;
		ctrl.getList(url);
	};
	
	this.confirmDeletion = function(donorId) {
		ctrl.donorIdForDeletion = donorId;
		jQuery('#deleteItemModalDialog').modal();
	};
	
	this.deleteSelectedDonor = function() {
		$http.delete(ctrl.deletePath(ctrl.donorIdForDeletion))
		.success(function(data) {
			ctrl.flushList();
			jQuery('#deleteItemModalDialog').modal('toggle');
		})
		.error(function(data) {
		});
	};
	
	this.flushList = function() {
		$http.delete(ctrl.urls.defaultList)
		.success(function(data) {
			// Reload list
			ctrl.getList(ctrl.urls.defaultList);
		})
		.error(function(data) {
		});
	};
};