/*jslint node: true */
/*global angular, jQuery */
'use strict';

/**
 * The DonorFormController manages the new and edit forms for Donors.
 * 
 * @author olimination
 * @since 20150112
 */
module.exports = function(CountryService) {
	var ctrl = this;
	ctrl.countryList = [];
	ctrl.country = {
		id: null,
		selected: ""
	};

	this.setCountryId = function(countryId) {
		ctrl.country.id = countryId;
	};
	
	this.setCurrentCountryItem = function() {
		angular.forEach(ctrl.countryList, function(value, key) {
			if(ctrl.country.id == value.nodeId) {
				ctrl.country.selected = value;
			}
		});
	};
	
	this.onSelectCountry = function(item, model) {
		console.log(item);
		jQuery("#address_country_nodeId").val(item.nodeId);
	};
	
	this.initList = function() {
		CountryService.getCountries()
		.success(function(data) {
			ctrl.countryList = data;
			ctrl.setCurrentCountryItem();
		})
		.error(function() {
			ctrl.countryList = [];
		});
	};
};