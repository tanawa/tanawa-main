/**
 * The Godchild module manages the children.
 * 
 * @author oliver.burkhalter
 * @date 20141109
 */
/*jslint node: true */
'use strict';

var appModule = require('angular').module('org.tanawa.godchild', []);
appModule.controller('GodchildController', ['$scope', '$http', require('./GodchildController')]);
appModule.controller('GodchildFormController', ['$filter', '$http', require('./GodchildFormController')]);

module.exports = appModule;