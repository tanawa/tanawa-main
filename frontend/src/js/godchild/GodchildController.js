/*jslint node: true */
/*global jsRoutes, jQuery */
'use strict';

module.exports = function($scope, $http) {
	var ctrl = this;
	ctrl.list = [];
	ctrl.isLoading = false;
	ctrl.godchildIdForDeletion = null;
	ctrl.urls = {
		defaultList: ""
	};

	this.detailPath = function(nodeId) {
		return jsRoutes.controllers.Godchildren.detailView(nodeId).url;
	};
	
	this.editPath = function(nodeId) {
		return jsRoutes.controllers.Godchildren.editView(nodeId).url;
	};
	
	this.deletePath = function(nodeId) {
		return jsRoutes.controllers.Godchildren.delete(nodeId).url;
	};

	this.searchInList = function(query, pathToList) {
		if (!query.length) {
			this.getList(pathToList);
		} else if (query.length > 2) {
			ctrl.isLoading = true;
			this.getList(jsRoutes.controllers.Godchildren.find(query).url);
		}
	};

	/**
	 * Loads the appropriate list data with the given url/path.
	 * This is used in the list view.
	 */
	this.getList = function(path) {
		ctrl.isLoading = true;
		$http.get(path).success(function(data) {
			ctrl.list = data;
			ctrl.isLoading = false;
		}).error(function() {
			ctrl.list = [];
			ctrl.isLoading = false;
		});
	};

    this.resetList = function(path) {
        $scope.query = "";
        this.getList(path);
    };
	
	this.initDefaultList = function(url) {
		ctrl.urls.defaultList = url;
		ctrl.getList(url);
	};
	
	this.confirmDeletion = function(godchildId) {
		ctrl.godchildIdForDeletion = godchildId;
		jQuery('#deleteItemModalDialog').modal();
	};
	
	this.deleteSelectedGodchild = function() {
		$http.delete(ctrl.deletePath(ctrl.godchildIdForDeletion))
		.success(function(data) {
			ctrl.flushList();
			jQuery('#deleteItemModalDialog').modal('toggle');
		})
		.error(function(data) {
		});
	};
	
	this.flushList = function() {
		$http.delete(ctrl.urls.defaultList)
		.success(function(data) {
			// Reload list
			ctrl.getList(ctrl.urls.defaultList);
		})
		.error(function(data) {
		});
	};
};