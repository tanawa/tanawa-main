/*jslint node: true */
'use strict';

var FileUtils = require("../fileUtils");
var FormMessages = require("../forms").FormMessages;

/**
 * The GodchildFormController manages the new and edit forms for Godchildren.
 * Default date format is 'yyyy-MM-dd'.
 * 
 * @author olimination
 * @since 20150115
 */
module.exports = function($filter, $http) {
	var ctrl = this;
	ctrl.dateFormat = "yyyy-MM-dd";
	ctrl.initDate = null;
	ctrl.currentDate = new Date();
	ctrl.selectedDate = null;
	ctrl.calendarOpened = false;
	ctrl.isSubmitting = false;

	this.openCalendarPopup = function($event) {
		$event.preventDefault();
		$event.stopPropagation();
		ctrl.calendarOpened = true;
	};

	this.setDateFormat = function(dateFormat) {
		ctrl.dateFormat = dateFormat;
	};

	this.setInitialDateByString = function(initDateString) {
		if (initDateString !== "") {
			ctrl.initDate = Date.parse(initDateString);

			// Check if date parsing was successfull
			if (!isNaN(ctrl.initDate)) {
				ctrl.setInitialDate(ctrl.initDate);
			}
		}
	};
	
	this.setInitialDate = function(dateObj) {
		ctrl.initDate = dateObj;
		
		// Workaround fix for:
		// - http://stackoverflow.com/questions/22660191/angularjs-ui-bootstrap-datepicker-date-format-in-controller-is-not-correct
		// - https://github.com/angular-ui/bootstrap/issues/2659m
		ctrl.selectedDate = $filter('date')(ctrl.initDate, ctrl.dateFormat);
	};

	this.sendFormData = function() {
		var actionUrl = jQuery("#godchildForm").attr("action");
		var profileImgDataUri = jQuery("#cropPreviewImage").attr("src");
		var profileImgBlob = null;

		if (profileImgDataUri) {
			profileImgBlob = FileUtils.dataURItoBlob(profileImgDataUri, "image/png");
		}

		var formElement = document.getElementById("godchildForm");
		var formData = new FormData(formElement);

		// Append cropped profile image
		if(profileImgBlob) {
			formData.append("profileImageFile", profileImgBlob, "profileImageFile.png");
		}

		// Init loading icon
		ctrl.isSubmitting = true;
		
		$http.post(actionUrl, formData, {
			// Don't do any request transformation
            transformRequest: angular.identity,
            headers: {
            	// Set to undefined
            	// Because the mulitpart/form-data will be set automatically with correct boundary
            	'Content-Type': undefined
            }
        }).success(function(data, status, headers, config) {
        	
        	ctrl.isSubmitting = false;
        	
        	// Show success messages
        	FormMessages.displaySuccess("godchildForm", data);
        	
        	// Update interface with new updated data
        	if(data.entity) {
        		angular.forEach(data.entity, function(value, key) {
        			if(value == 0) {
        				jQuery("#" + key).val("");
        			} else if (key == "dateBorn" && value != null) {
        				ctrl.setInitialDate(new Date(value));
        				jQuery("#" + key).val(ctrl.selectedDate);
        			} else if (value != null) {
        				jQuery("#" + key).val(value);	
        			} else if (value == null) {
        				jQuery("#" + key).val("");
        			}
        		});

        		// Update text in page title
        		jQuery("span.child-name").text(data.entity.firstName + " " + data.entity.lastName);
        	}
        	
        	// Reset crop image preview
        	jQuery("#cropPreviewImage").hide();
        	
        	// Update profile image view
        	var refreshProfileImageUrl = jQuery("#defaultProfileImage").attr('src') + "?" + new Date().getTime();
        	jQuery("#defaultProfileImage").attr('src', "");
        	jQuery("#defaultProfileImage").attr('src', refreshProfileImageUrl);
        	jQuery("#defaultProfileImage").show();
        	
        	
        }).error(function(data, status, headers, config) {
        	
        	// Redirect to login page if Not Authorized
        	if(status === 401) {
        		location.reload(false);
        	}
        	
        	ctrl.isSubmitting = false;
        	
        	// Display form error messages
        	FormMessages.displayError(data);
        });
	};
};