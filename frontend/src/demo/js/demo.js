'use strict';

// Define the global jsRoutes object
// This object is built by the Play Framework (Backend) and is mocked here for the Frontend prototype
var jsRoutes = {
	controllers: {
		Donors: {
			getById: function(nodeId) {
				return {url: "demo/mocks/donors/" + nodeId + ".json"};
			},
			supportGodchild: function(donorId, godchildId) {
				return {url: "demo/mocks/donors/supportGodchild.json?donorId=" + donorId + "&godchildId=" + godchildId};
			},
			endSupportGodchild: function(donorId, godchildId) {
				return {url: "demo/mocks/donors/endSupportGodchild.json?donorId=" + donorId + "&godchildId=" + godchildId};
			},
			delete: function(nodeId) {
				return {url: "demo/mocks/donors/" + nodeId + ".json"};
			}
		}
	}
};
