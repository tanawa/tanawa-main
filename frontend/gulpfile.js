var gulp = require('gulp');
var del = require('del');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var streamify = require('gulp-streamify');
var minifyCSS = require('gulp-minify-css');
var ngAnnotate = require('gulp-ng-annotate');
var connect = require('gulp-connect');


var paths = {
	scripts : [ 'src/js/**/*.js' ],
	demoScripts : [ 'src/demo/**/*.js', 'src/demo/**/*.json' ],
	styles:   [ 'src/css/bootstrap.min.css', 
	            'src/css/metisMenu.min.css', 
	            'src/css/sb-admin-2.css', 
	            'src/css/font-awesome.min.css',
	            'src/css/select.min.css',
	            'src/css/jquery.Jcrop.min.css',
	            'src/css/main.css' ],
	html :    [ 'src/*.html' ],
	assets:   [ 'src/files/**/*' ],
	target :  'target/',
	targetJs: 'target/js',
	targetStyles: 'target/css',
	targetNgMin: './target/ngmin',
	targetPlayDir: '../webapp/public'
};

// Default task
gulp.task('default', [ 'clean', 'lint', 'browserify', 'styles', 'html', 'assets' ], function() {
});

// Copy demo/prototype files to target
// This is used for mocking js data, etc.
gulp.task('demo', function() {
	return gulp.src(['src/demo/**/*'])
		.pipe(gulp.dest('target/demo'));
});

// Clean task
gulp.task('clean', function () {  
	del(['target/**', 'target']);
});

// Lint task
gulp.task('lint', function() {
	return gulp.src(paths.scripts).pipe(jshint()).pipe(jshint.reporter('default'));
});

// Browserify
gulp.task('browserify', function() {
	return browserify('./src/js/app.js', { debug: true })
		.bundle()
		.pipe(source('main.js'))
		.pipe(gulp.dest(paths.targetJs))
});

// ng-annotate task, makes angular code compatible for minified version
gulp.task('ngmin', function() {
	return gulp.src(paths.scripts)
		.pipe(ngAnnotate())
		.pipe(gulp.dest(paths.targetNgMin));
});

// Browserify minified
gulp.task('browserify-min', [ 'ngmin' ], function() {
	return browserify(paths.targetNgMin + '/app.js')
		.bundle()
		.pipe(source('main.min.js'))
		.pipe(streamify(uglify({ mangle: false })))
		.pipe(gulp.dest(paths.targetJs));
});

// Styles minified task
gulp.task('styles-min', function() {
	return gulp.src(paths.styles)
		.pipe(concat('main.min.css'))
		.pipe(minifyCSS())
		.pipe(gulp.dest(paths.targetStyles));
});

// Styles task
gulp.task('styles', function() {
	return gulp.src(paths.styles)
		.pipe(concat('main.css'))
		.pipe(gulp.dest(paths.targetStyles));
});

// Copy Static assets task
gulp.task('assets', function() {
	return gulp.src(paths.assets)
		.pipe(gulp.dest(paths.target));
});

// HTML task
gulp.task('html', function() {
	return gulp.src(paths.html)
		.pipe(gulp.dest(paths.target));
});

// Server task
gulp.task('server', ['build'], function() {
	connect.server({
		root: 'target',
		livereload: true,
	});
});

// Watch task for Frontend development
gulp.task('watch', function() {
	gulp.start('server');
	gulp.watch([paths.html, paths.styles, paths.scripts, paths.demoScripts], ['build', 'demo']);
});

// Watch task for Play development
gulp.task('watch:play', function() {
	gulp.start('server');
	gulp.watch([paths.html, paths.styles, paths.scripts], ['deploy:play']);
});

// Build task for non-minified version
gulp.task('build', ['browserify', 'styles', 'html', 'assets'], function() {
});

// Dist task for minified version
gulp.task('dist', ['browserify-min', 'styles-min', 'html', 'assets'], function() {
});

// Deploy to Play public assets directory
gulp.task('deploy:play', ['build', 'dist'], function() {
	gulp.src(['target/js/**/*'])
		.pipe(gulp.dest(paths.targetPlayDir + '/js'));
	gulp.src(['target/css/**/*'])
		.pipe(gulp.dest(paths.targetPlayDir + '/css'));
});