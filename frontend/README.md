## Tanawa - Frontend app

The Tanawa Frontend app is based on NPM, Browserify and GulpJS as build system.

Directory structure:

- src/ -> all source files
  - css/
  - files/ -> static files
  - js/
  - vendor/ -> custom libs which are not defined by npm package.json, e.g. custom angularjs version
  - *.html
  
- target/ -> Contains all built, non-minified and minified files


## Setup
- Install Node.js
- Execute: npm install
    
## How to build

### Build all non-minified (default task)

    gulp
    
### Build all minified

    gulp dist

### Start local server and watch files (js, css, html) (localhost:8080) for Frontend development

    gulp watch
    
### Watch and deploy automatically changes (js, css) to Play webapp

    gulp watch:play
